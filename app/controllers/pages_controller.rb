class PagesController < ApplicationController
  @@home = "/"
	
  def home
    @topnav = 'pages/topnav'
  end

  def contact
    @topnav = 'pages/topnav'
    message = {}
    message[:name] = params[:name] ? params[:name] : nil
    message[:email] = params[:email] ? params[:email] : nil
    message[:body] = params[:message] ? params[:message] : nil
    @test = nil
    @results = 'Fill in the following form:' 

    if message[:email] != nil
      @test = UserMailer.test_mailer(message).deliver
      @results = @test.to ? "#{message[:name]} your contact was successful." : 'Fill in the following form:'
    end
  end

end
